# stock-trading-app

A stock trading app that allows users to maintain a portfolio, buy and sell stocks.

## Usage instructions

Install
---
Make sure you have NodeJS v10.13.0 installed.
Clone the repository or extract the source code zip file.
Run the command below to install the dependencies.

```
npm install
```

Run
---

```
npm start
```

Console output should show that the app is running along with the port number.
Open your browser and go to
```
http://localhost:1337
```

Debug
---

To run in debug mode

```
npm install -g nodemon
nodemon
```

Hosting
---

```
https://stock-trading-application.herokuapp.com/
```

Usage Info
---

Login using the test credentials provided


Credits
---

Alpha Vantage API for Stock Data

Couldn't have done without these amazing libraries
```
async
moment
request
bootstrap
express
nodemon
```
