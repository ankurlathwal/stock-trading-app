global.__base       = __dirname + '/';
const path          = require('path');
const express       = require('express');
const app           = express();
const PORT          = 1337;
const server        = require('http').Server(app);


app.use(express.static('./public'));

// all routes
require('./server/routes')(app);

// by default - send the index.html file
app.get('*', function(req, res){
    res.sendFile(path.join(__dirname, './public/app/index.html'));
});

console.log('Stock Trading App is running \nListening on Port: ' + PORT);
server.listen(process.env.PORT || PORT);

