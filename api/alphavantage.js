// Service that calls the Alpha Vantage API

const request           = require('request');

const config            = require('../server/config').config;
const endpoint          = config.alphavantage.endpoint;

const apikey            = config.alphavantage.apikey;
var url                 = endpoint + '?' + 'apikey=' + apikey;

module.exports.getStock = function(symbol, callback){
        url = url + '&function=GLOBAL_QUOTE&interval=60min&symbol=' + symbol;
        request({url: url, method: 'GET'}, function(err, response){
            if(err) callback(err);
            else{
                callback(null, JSON.parse(response.body));
            }
        })

}

module.exports.searchStock = function(keywords, callback){
    url = url + '&function=SYMBOL_SEARCH&keywords=' + keywords;
    request({url: url, method: 'GET'}, function(err, response){
        if(err) callback(err);
        else{
            callback(null, JSON.parse(response.body));
        }
    })
}