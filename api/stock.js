// The local stock API

const StockService          = require('./alphavantage');

module.exports.find = function(req, res){
    res.status(200).json({"id":1, "value": 399.05});
}

module.exports.getStockBySymbol = function(req, res){
    var symbol = req.query.symbol ? req.query.symbol : '';
    StockService.getStock(symbol, function(err, stockData){
        if(err){
            res.status(400).json(err);
        }
        else{
            res.status(200).json(stockData);
        }
    })
}

module.exports.searchStock = function(req, res){
    var keywords = req.query.keywords ? req.query.keywords : '';
    StockService.searchStock(keywords, function(err, stockResults){
        if(err){
            res.status(400).json(err);
        }
        else{
            res.status(200).json(stockResults);
        }
    })
}