// All the API routes

const Stocks            = require('../api/stock');

module.exports = function (app) {
    app.get('/api/stock', Stocks.getStockBySymbol);
    app.get('/api/stock/search', Stocks.searchStock);
}
