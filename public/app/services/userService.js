/* Contains cookies storage and retrieval functions and test user accounts*/

app.factory('User', ['$cookies', function($cookies){
    var userFactory = {};

    userFactory.setUser = function(user){
        if(!user.name) user.name = "TestUser";
        if(!user.userStocks) user.userStocks = [];
        if(!user.balance) user.balance = 0;
        $cookies.put('user', JSON.stringify(user));
    }

    userFactory.getUser = function(){
        return JSON.parse($cookies.get('user'));
    }

    userFactory.data = [
        {email: 'test@qwilr.com', password: 'qwilr'},
        {email: 'alathwal@outlook.com', password: 'test'}
    ];

    return userFactory;
}])