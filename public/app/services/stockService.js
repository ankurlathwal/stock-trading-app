/* Call the API of the application */

app.factory('StockService', ['$http', function($http){
    var baseURI = '/api/stock';

    var stockFactory = {};

    stockFactory.searchByKeyword = function(keyword){
        return $http.get(baseURI + '/search?keywords=' + keyword);
    }

    stockFactory.getStockBySymbol = function(symbol){
        return $http.get(baseURI + '?symbol=' + symbol);
    }

    return stockFactory;
}])