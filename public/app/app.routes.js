app.config(function($stateProvider, $urlRouterProvider, $locationProvider){

    $stateProvider

        .state('home', {
            url: '/',
            templateUrl: 'app/views/home.html',
            controller: 'homeController'
        })

        .state('login', {
            url: '/login',
            templateUrl: 'app/views/login.html',
            controller: 'loginController'
        })

    $urlRouterProvider.otherwise('/login')    
})