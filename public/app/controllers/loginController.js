app.controller('loginController', function($scope, $state, $cookies, User){
    $scope.loginUser = function(){
        $scope.invalidLogin = false;
        User.data.forEach(function(record){
            if(record.email === $scope.email && record.password === $scope.password){
                var name = $scope.email.indexOf('qwilr') > -1 ? 'Qwilr' : 'Ankur';
                var user = {
                    name:  name,
                    balance: 10000,
                    userStocks: [
                        { symbol: "AAPL", quantity: 4 }
                    ]
                };
                User.setUser(user);
                $state.go('home');
            }
        })
        $scope.invalidLogin = true;
    }
})