app.controller('homeController', function($scope, StockService, User){
   

    function init() {
        $scope.searchKeywords = "";
        $scope.searchResults = [];
        var userData = User.getUser();
        $scope.negativeStock = false;
        $scope.availableBalance = userData.balance;
        $scope.user = {
            firstName: userData.name
        }
        $scope.userStocks = userData.userStocks;
        calculateUserStockValues();
        getCurrentTime();
        
    }

    $scope.searchStocks = function(){
        if($scope.searchKeywords){
            StockService.searchByKeyword($scope.searchKeywords).then(function(success){
                $scope.searchResults = success.data.bestMatches;
            }, function(err){
                console.log(err);
            })
        }
    }

    $scope.openStockModal = function(stock){
        $scope.selectedStock = stock;
        StockService.getStockBySymbol(stock["1. symbol"]).then(function(success){
            $scope.selectedStock.quote = success.data["Global Quote"];
            console.log($scope.selectedStock.quote);
        }, function(err){
            console.log(err);
        })
    }

    $scope.calculateStockWorth = function(selectedStockQuantity){
        if(selectedStockQuantity < 0){
            $scope.negativeStock = true;
            $scope.overlimit = false;
        }
        else{
            $scope.negativeStock = false;
            $scope.stockWorth = selectedStockQuantity*$scope.selectedStock.quote["02. open"];
            if($scope.stockWorth > $scope.availableBalance){
                $scope.overlimit = true;
            }
            else{
                $scope.overlimit = false;
            }
        }
        
        
    }

    function calculateUserStockValues(){
        // Run through all user stocks and fetch their current values
        if($scope.userStocks.length){
            async.eachSeries($scope.userStocks, function(stock, next){
                StockService.getStockBySymbol(stock.symbol).then(function(success){
                    if(success.data && success.data["Global Quote"] && success.data["Global Quote"]["02. open"]){
                        stock.value = success.data["Global Quote"]["02. open"];
                        stock.worth = stock.value * stock.quantity;
                        $scope.apiError = false;
                    }
                    else{
                        $scope.apiError = true;
                    }
                    next();
                }, function(err){
                    console.log(err);
                    $scope.apiError = true;
                    next();
                })
            }, function(){
                calculateNetWorth();
            })
        }
        
    }

    function calculateNetWorth(){
        if($scope.userStocks.length){
            $scope.netWorth = 0;
            $scope.totalStocks = 0;
            $scope.userStocks.forEach(function(stock){
                $scope.netWorth += stock.worth || 0;
                $scope.totalStocks += stock.quantity || 0;
            })
        }
    }

    function getCurrentTime(){
        $scope.currentTime = moment().format('LLLL');
    }

    $scope.buySelectedStock = function(selectedStockQuantity){
        $scope.availableBalance -= $scope.stockWorth;
        // Push the newly purchased stock to user stocks
        var newStock = {
            symbol: $scope.selectedStock["1. symbol"],
            quantity: selectedStockQuantity,
            value: $scope.selectedStock.quote["02. open"],
            worth: $scope.stockWorth
        }

        pushStock(newStock);
        calculateNetWorth();
        updateUserData();
    }

    function pushStock(newStock){
        var merged = false;
        $scope.userStocks.forEach(function(oldStock){
            if(oldStock.symbol === newStock.symbol){
                oldStock.quantity += newStock.quantity;
                oldStock.value = newStock.value;
                oldStock.worth += newStock.worth;
                merged = true;
            }
        })
        if(!merged) $scope.userStocks.push(newStock);
        
    }

    $scope.openSellModal = function(stock){
        $scope.sellStock = stock;
    }

    $scope.checkSellWorth = function(sellStockQuantity){
        if(sellStockQuantity > $scope.sellStock.quantity || sellStockQuantity < 1){
            $scope.sellOverLimit = true;
        }
        else{
            $scope.sellPrice = sellStockQuantity * $scope.sellStock.value;
            $scope.sellOverLimit = false;
        }

    }

    $scope.sellStockValue = function(sellStockQuantity){
        $scope.availableBalance += $scope.sellStock.value * sellStockQuantity;
        $scope.userStocks.forEach(function(userStock){
            if(userStock.symbol === $scope.sellStock.symbol){
                userStock.quantity -= sellStockQuantity;
                userStock.worth = userStock.quantity*userStock.value;
            }
        })
        cleanUpStockList();
        calculateNetWorth();
        updateUserData();
    }

    function cleanUpStockList(){
        $scope.userStocks = $scope.userStocks.filter(function(stock){
            return stock.quantity > 0;
        })
    }

    $scope.addFunds = function(amount){
        if(amount){
            $scope.availableBalance += amount;
            updateUserData();
        }
    }

    // Update the cookie storage values for user
    function updateUserData(){
        console.log('Updating User data');
        var user = {
            name: $scope.user.firstName,
            balance: $scope.availableBalance,
            userStocks: $scope.userStocks
        };
        User.setUser(user);
    }
    
    init();
})